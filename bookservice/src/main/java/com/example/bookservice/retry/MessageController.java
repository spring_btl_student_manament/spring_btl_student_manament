package com.example.bookservice.retry;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.github.bucket4j.Refill;
import org.springframework.retry.annotation.Backoff;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@RestController
public class MessageController {

    Logger logger = LoggerFactory.getLogger(MessageController.class);

    private int i = 1;
    private long lastCallTime = 0L;
    private long timeDifference = 0L;

    // Thiết lập Bucket cho rate limiting
    private final Bucket bucket;

    public MessageController() {
        // Giới hạn 10 yêu cầu mỗi phút
        Bandwidth limit = Bandwidth.classic(5, Refill.greedy(10, Duration.ofMinutes(1)));
        this.bucket = Bucket4j.builder()
                .addLimit(limit)
                .build();
    }

    @GetMapping("/ratelimiter")
    public String getRatelimiter() {
        if (bucket.tryConsume(1)) {
            timeDifference = System.currentTimeMillis() - lastCallTime;
            logger.info(" Đang gọi Api lần: " + i++ + " Thời gian chờ: " + timeDifference + " ms");
            RestTemplate rt = new RestTemplate();
            lastCallTime = System.currentTimeMillis();
            rt.getForObject("http://localhost:9002/api/v1/employees", Object.class);
            return "Gọi Api thành công!";
        } else {
            logger.warn("Quá nhiều yêu cầu, vui lòng thử lại sau.");
            return "Quá nhiều yêu cầu, vui lòng thử lại sau.";
        }
    }
    
    
  @GetMapping("/retry")
  @Retryable(maxAttempts = 10, backoff = @Backoff(delay = 2000, multiplier = 2))
  
  public String getMessage(){
      timeDifference = System.currentTimeMillis() - lastCallTime;
      logger.info(" Đang gọi Api Lần :  "+ i++ + " Thời gian chờ : " + timeDifference + " ms");
      RestTemplate rt = new RestTemplate();
      lastCallTime = System.currentTimeMillis();
      rt.getForObject("http://localhost:9002/api/v1/employees", Object.class);
      logger.info("Gọi Api thành công !");
      return "Gọi Api thành công !";
  }

    @Recover
    public String getRecoveryMessage() {
        logger.info(" Không thể gọi Api !");
        return "Không thể gọi Api!";
    }
}